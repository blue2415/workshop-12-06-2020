const rewire = require('rewire');
let demo = require('../src/demo');

describe('demo', () => {
  describe('add', () => {
    it('should add two number', () => {
      expect(demo.add(1, 2)).toBe(3);
    });
  });

  describe('callback add', () => {
    it('should test the callback', (done) => {
      expect(
        demo.addCallback(1, 1, (err, result) => {
          expect(err).toBeNull();
          expect(result).toBe(2);
          done();
        })
      );
    });
  });

  describe('test promise', () => {
    test('should add the promise', (done) => {
      demo
        .addPromise(1, 2)
        .then((result) => {
          expect(result).toBe(3);
          done();
        })
        .catch((error) => {
          done(error);
        });
    });

    test('should add promise with async/await', async () => {
      const result = await demo.addPromise(1, 2);
      expect(result).toBe(3);
    });

    test('should add promise with return', () => {
      return demo.addPromise(1, 2).then((result) => {
        expect(result).toBe(3);
      });
    });

    test('should add promise with inline await', async () => {
      await expect(demo.addPromise(1, 2)).resolves.toBe(3);
    });
  });

  describe('test doubles', () => {
    test('should spy on log', () => {
      const mySpy = jest.spyOn(console, 'log');
      demo.foo();
      expect(mySpy).toHaveBeenCalledTimes(1);
    });

    test('should stub/mocking on warn', () => {
      const myStub = jest.spyOn(console, 'warn');
      myStub.mockImplementation(() => {
        console.log('message from mocking/stub');
      });
      demo.foo();
      expect(myStub).toHaveBeenCalledTimes(1);
    });
  });

  describe('stub private function', () => {
    test('should stub createFile', async () => {
      demo = rewire('../src/demo');
      let spyCreateFile = jest
        .spyOn(demo, 'createFile')
        .mockResolvedValueOnce('create stub');
      let callStub = jest.fn().mockResolvedValueOnce('calldb_stub');

      demo.__set__('callDB', callStub);
      let result = await demo.bar('test.txt');

      expect(result).toBe('calldb_stub');
      expect(spyCreateFile).toHaveBeenCalledTimes(1);
      expect(spyCreateFile).toHaveBeenCalledWith('test.txt');
      expect(callStub).toHaveBeenCalledTimes(1);
    });
  });
});
